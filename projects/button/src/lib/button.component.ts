/*
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-button',
  template: `
    <p>
      button works!
    </p>
  `,
  styles: []
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
*/


import { Component, Input } from '@angular/core';
import { select2 } from 'select2';

@Component({
  selector: 'lib-button',
  template: `
    <button [disabled]="disabled">{{text}}</button>
  `
})
export class ButtonComponent {
  @Input() text: string;
  @Input() disabled: boolean;
}
