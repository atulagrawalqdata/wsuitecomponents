import { Component, OnInit, Input } from '@angular/core';
import { NgSelect2Module } from "ng-select2";

@Component({
  selector: 'ui-wselect',
  template: `
  <select>
    <option>select option</option>
  </select>
  `,
  styles: []
})

//<ng-select2 [data]="exampleData" [placeholder]="{{placeholder}}"></ng-select2>

export class WselectComponent implements OnInit {
  @Input() data: String;
  @Input() value: String;
  @Input() width: String;
  @Input() disabled: boolean;
  @Input() placeholder: String;

  constructor() { }

  ngOnInit() {
  }

}
