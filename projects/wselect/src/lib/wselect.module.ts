import { NgModule } from '@angular/core';
import { WselectComponent } from './wselect.component';

@NgModule({
  declarations: [WselectComponent],
  imports: [
  ],
  exports: [WselectComponent]
})
export class WselectModule { }
