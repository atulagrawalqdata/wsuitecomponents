import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WselectComponent } from './wselect.component';

describe('WselectComponent', () => {
  let component: WselectComponent;
  let fixture: ComponentFixture<WselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
