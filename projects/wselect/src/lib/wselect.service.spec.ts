import { TestBed } from '@angular/core/testing';

import { WselectService } from './wselect.service';

describe('WselectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WselectService = TestBed.get(WselectService);
    expect(service).toBeTruthy();
  });
});
