import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WsuiteDropdownComponent } from './wsuite-dropdown/wsuite-dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    WsuiteDropdownComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
