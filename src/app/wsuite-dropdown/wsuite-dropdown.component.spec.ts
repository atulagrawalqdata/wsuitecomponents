import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WsuiteDropdownComponent } from './wsuite-dropdown.component';

describe('WsuiteDropdownComponent', () => {
  let component: WsuiteDropdownComponent;
  let fixture: ComponentFixture<WsuiteDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WsuiteDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WsuiteDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
