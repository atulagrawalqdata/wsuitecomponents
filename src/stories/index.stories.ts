import { storiesOf } from '@storybook/angular';
import { Welcome, Button } from '@storybook/angular/demo';
import { linkTo } from '@storybook/addon-links';
import { action } from '@storybook/addon-actions';

import { ButtonComponent } from 'button';
import { WselectComponent } from 'wselect';

import { environment } from '../environments/environment';
if(environment){
}

storiesOf('Welcome', module).add('to Storybook', () => ({
  component: Welcome,
  props: {},
  /*
  template: `<storybook-welcome-component (showApp)="showApp()"></storybook-welcome-component>`,
  props: {
    showApp: linkTo('Button'),
  },
  moduleMetadata: {
    declarations: [Welcome],
  },
  */
}));

storiesOf('W-Select', module)
  .add('Default', () => ({
    component: WselectComponent,
    props: {
      placeholder: 'Select'
     }
}));

storiesOf('button', module)
  .add('basic', () => ({
    component: ButtonComponent,
    props: {
      text: 'hello'
    }
  }))
  .add('disabled', () => ({
    component: ButtonComponent,
    props: {
      text: 'disabled',
      disabled: true
    }
  }));


storiesOf('Button', module)
  .add('with text', () => ({
    component: Button,
    props: {
      text: 'Hello Button',
    },
  }))
  .add(
    'with some emoji',
    () => ({
      component: Button,
      props: {
        text: '😀 😎 👍 💯',
      },
    }),
    { notes: 'My notes on a button with emojis' }
  )
  .add(
    'with some emoji and action',
    () => ({
      component: Button,
      props: {
        text: '😀 😎 👍 💯',
        onClick: action('This was clicked OMG'),
      },
    }),
    { notes: 'My notes on a button with emojis' }
  );

storiesOf('Another Button', module).add('button with link to another story', () => ({
  component: Button,
  props: {
    text: 'Go to Welcome Story',
    onClick: linkTo('Welcome'),
  },
}));
